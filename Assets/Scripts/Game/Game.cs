﻿using UnityEngine;


public class Game : MonoBehaviour
{
    [SerializeField]
    private Shooter shooter = default;
    [SerializeField]
    private Enemy enemy = default;


    private void Awake()
    {
        enemy.Initialize();
        shooter.Initialize(enemy);
    }
}
