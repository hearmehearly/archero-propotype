﻿using UnityEngine;


public class ShooterMovement : RectMonoBehavior
{
    private Enemy target;
    private float speed;

    private Rect gameZoneRect;


    public void Initialize(Enemy targetValue, float speedValue)
    {
        target = targetValue;
        speed = speedValue;

        gameZoneRect = CommonUtility.GetGameZoneRect();
    }


    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        float translationY = Input.GetAxis("Vertical") * Time.deltaTime * speed;
        float translationX = Input.GetAxis("Horizontal") * Time.deltaTime * speed;

        Vector3 translateVector = new Vector3(translationX, translationY, default);
        Vector3 nextFramePosition = transform.position + transform.TransformDirection(translateVector);

        Rect nextFrameRect = Rect;
        nextFrameRect.center = nextFramePosition;

        bool allowMovement = gameZoneRect.Contains(nextFramePosition);
        allowMovement &= (target == null ||
                         (target != null && !nextFrameRect.Overlaps(target.Rect)));

        if (allowMovement)
        {
            transform.Translate(translateVector);
        }
        
        Vector3 aimVector = (target != null) ? (target.transform.position - transform.position) : Vector3.up;
        float angle = Mathf.Atan2(-aimVector.x, aimVector.y) * Mathf.Rad2Deg;
        transform.eulerAngles = new Vector3(0f, 0f, angle);
    }
}
