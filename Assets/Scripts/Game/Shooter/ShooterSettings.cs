﻿using UnityEngine;


[CreateAssetMenu]
public class ShooterSettings : ScriptableObject
{
    public float movementSpeed = default;
    public Projectile projectilePrefab = default;
    public float projectileSpeed = default;
    public float projectileDamage = default;


    private void OnValidate()
    {
        movementSpeed = (movementSpeed < (float)default) ? default : movementSpeed;
        projectileSpeed = (projectileSpeed < (float)default) ? default : projectileSpeed;
        projectileDamage = (projectileDamage < (float)default) ? default : projectileDamage;
    }
}
