﻿using UnityEngine;
using System;


public class ShooterWeapon : MonoBehaviour
{
    public static event Action OnShoot;

    private Projectile projectilePrefab;
    private float projectileSpeed;
    private float projectileDamage;

    private Transform enemy;


    // separate data on weapon data/movement data and initialiwe with class
    public void Initialize(Projectile projectilePrefabValue,
                           float projectileSpeedValue,
                           float projectileDamageValue,
                           Transform enemyValue)
    {
        projectilePrefab = projectilePrefabValue;
        projectileSpeed = projectileSpeedValue;
        projectileDamage = projectileDamageValue;

        enemy = enemyValue;
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (projectilePrefab == null)
            {
                Debug.LogWarning("No projectile prefab for shooter");
            }
            else
            {
                Projectile projectile = Instantiate(projectilePrefab, transform.position, Quaternion.identity);
                Vector3 shotDirection = (enemy != null) ? enemy.position - transform.position : Vector3.up;
                projectile.Initialize(shotDirection, projectileSpeed, projectileDamage);

                OnShoot?.Invoke();
            }
        }
    }
}
