﻿using UnityEngine;


public class Shooter : MonoBehaviour
{
    [SerializeField]
    private ShooterSettings settings = default;
    
    [SerializeField]
    private ShooterMovement movement = default;
    [SerializeField]
    private ShooterWeapon weapon = default;


    public void Initialize(Enemy enemy)
    {
        movement.Initialize(enemy, settings.movementSpeed);

        weapon.Initialize(settings.projectilePrefab,
                          settings.projectileSpeed,
                          settings.projectileDamage,
                          enemy.transform);
    }
}
