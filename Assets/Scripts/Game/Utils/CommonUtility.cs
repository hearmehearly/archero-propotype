﻿using UnityEngine;


public static class CommonUtility
{
    public static Rect GetGameZoneRect()
    {
        Camera gameCamera = Camera.main;
        float cameraHalfWidth = gameCamera.orthographicSize * gameCamera.aspect;

        Vector2 cameraLowAnchor = new Vector2(gameCamera.transform.position.x - cameraHalfWidth, gameCamera.transform.position.y - gameCamera.orthographicSize);
        Vector2 cameraSize = new Vector2(cameraHalfWidth * 2.0f, gameCamera.orthographicSize * 2.0f);

        return new Rect(cameraLowAnchor, cameraSize);
    }
}
