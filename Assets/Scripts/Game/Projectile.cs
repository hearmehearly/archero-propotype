﻿using UnityEngine;


public class Projectile : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D mainRigidbody2D = default;
    private Rect gameZoneRect;


    public float Damage { get; private set; }


    public void Initialize(Vector3 direction, float velocityMagnitude, float damage)
    {
        float angleRadian = Mathf.Atan2(direction.y, direction.x);

        Vector2 shotVelocity = new Vector2()
        {
            x = velocityMagnitude * Mathf.Cos(angleRadian),
            y = velocityMagnitude * Mathf.Sin(angleRadian)
        };

        mainRigidbody2D.velocity = shotVelocity;

        Damage = damage;
        
        gameZoneRect = CommonUtility.GetGameZoneRect();
    }


    // should do this in controller in foreach (var projectile in activeProjectiles)...
    // use pool
    private void FixedUpdate()
    {
        if (!gameZoneRect.Contains(transform.position))
        {
            Destroy(gameObject);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy enemy = collision.GetComponent<Enemy>();

        if (enemy != null)
        {
            Destroy(gameObject);
        }
    }
}
