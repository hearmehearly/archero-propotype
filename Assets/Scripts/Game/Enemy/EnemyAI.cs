﻿using DG.Tweening;
using UnityEngine;


public class EnemyAI : MonoBehaviour
{
    private EnemySettings settings;

    private float initialPositionX;

    private float leftAvailablePosition;
    private float rightAvailablePosition;

    private float leftVelocityMagnitude;
    private float rightVelocityMagnitude;


    // into inintialize
    private void OnEnable()
    {
        ShooterWeapon.OnShoot += Move;
    }


    // into deinitiali
    private void OnDisable()
    {
        ShooterWeapon.OnShoot -= Move;
        DOTween.Kill(this);
    }



    public void Initialize(EnemySettings settingsValue)
    {
        settings = settingsValue;
        initialPositionX = transform.position.x;

        Camera gameCamera = Camera.main;
        float cameraHalfWidth = gameCamera.orthographicSize * gameCamera.aspect;

        float leftAvailableDistance = Mathf.Min(settings.moveDistance, transform.position.x - (gameCamera.transform.position.x - cameraHalfWidth));
        float rightAvailableDistance = Mathf.Min(settings.moveDistance, (gameCamera.transform.position.x + cameraHalfWidth) - transform.position.x);

        leftVelocityMagnitude = Mathf.Approximately(settings.moveVelocity, default) ? default : leftAvailableDistance / settings.moveVelocity;
        rightVelocityMagnitude = Mathf.Approximately(settings.moveVelocity, default) ? default : rightAvailableDistance / settings.moveVelocity;
        
        leftAvailablePosition = transform.position.x - leftAvailableDistance;
        rightAvailablePosition = transform.position.x + rightAvailableDistance;
    }


    public void Move()
    {  
        bool shouldMoveLeft = Random.value > 0.5f;
        
        float endPositionX = shouldMoveLeft ? leftAvailablePosition : rightAvailablePosition;
        float velocityMagnitude = shouldMoveLeft ? leftVelocityMagnitude : rightVelocityMagnitude;

        DOTween.Sequence()
            .AppendInterval(settings.moveDelay)
            .Append(transform.DOMoveX(endPositionX, velocityMagnitude).SetEase(Ease.Linear))
            .Append(transform.DOMoveX(initialPositionX, velocityMagnitude).SetEase(Ease.Linear))
            .SetId(this);
    }
}
