﻿using UnityEngine;


[CreateAssetMenu]
public class EnemySettings : ScriptableObject
{
    public float moveDelay = default;
    public float moveDistance = default;
    public float moveVelocity = default;

    public float health = default;
    public ParticleSystem deathEffect = default;


    private void OnValidate()
    {
        health = (health < (float)default) ? default : health;
        moveDelay = (moveDelay < (float)default) ? default : moveDelay;
        moveDistance = (moveDistance < (float)default) ? default : moveDistance;
        moveVelocity = (moveVelocity < (float)default) ? default : moveVelocity;
    }
}
