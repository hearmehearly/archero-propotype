﻿using System;
using UnityEngine;


public class EnemyHealthController : MonoBehaviour
{
    private event Action OnShouldDie;
    
    private float currentHealth;


    // separate settings by AI and health
    public void Initialize(float maxHelth)
    {
        currentHealth = maxHelth;
    }

    public void AddOnShouldDieCallback(Action callback)
    {
        OnShouldDie += callback;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Projectile projectile = collision.GetComponent<Projectile>();

        if (projectile != null)
        {
            currentHealth -= projectile.Damage;

            if (currentHealth <= 0.0f)
            {
                OnShouldDie?.Invoke();
                OnShouldDie = null;
            }
        }
    }
}
