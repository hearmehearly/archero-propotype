﻿using UnityEngine;


public class Enemy : RectMonoBehavior
{
    [SerializeField]
    private EnemySettings settings = default;

    [SerializeField]
    private EnemyHealthController healthController = default;
    [SerializeField]
    private EnemyAI enemyAI = default;


    public void Initialize()
    {
        healthController.Initialize(settings.health);
        healthController.AddOnShouldDieCallback(OnShouldDie);

        enemyAI.Initialize(settings);
    }


    private void OnShouldDie()
    {
        ParticleSystem effect = Instantiate(settings.deathEffect, transform.position, Quaternion.identity);

        if (effect != null)
        {
            // create custom effect handler and use pool
            effect.Play();
        }

        Destroy(gameObject);
    }
}
