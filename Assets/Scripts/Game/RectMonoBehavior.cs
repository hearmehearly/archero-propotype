﻿using UnityEngine;


public class RectMonoBehavior : MonoBehaviour
{
    [SerializeField]
    private Vector2 size = Vector2.zero;

    [HideInInspector] public Rect Rect;


    private void Awake()
    {
        Rect.size = size * transform.localScale;
    }


    protected virtual void FixedUpdate()
    {
        Rect.center = transform.position;
    }

    
    #if UNITY_EDITOR

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, size);
    }

    #endif
}
